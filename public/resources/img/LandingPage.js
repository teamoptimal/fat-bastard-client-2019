import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchPage, fetchGallery } from '../../actions' 
import SimpleSlider from '../slider/SimpleSlider'
import ButtonRight from '../buttons/ButtonRight'

class LandingPage extends Component {
  componentDidMount(){
    this.props.fetchGallery(1, 'slider')
  }

  render() {
    return (
      <div>
       <div className="ui container">
         <div className="home-slider">
            <SimpleSlider content={this.props.sliderGallery} />
          <ButtonRight to="/the-fat-bastards" color="yellow" title="View Our products"/>
         </div>
          
        </div>  
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { 
    page: state.page,
    sliderGallery: state.sliderGallery
   }
}

export default connect(mapStateToProps, {
  fetchPage,
  fetchGallery
})(LandingPage)