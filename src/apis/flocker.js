import axios from "axios";

export default axios.create({
  baseURL: "https://api.flockler.com/v1/sites/8286",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});
