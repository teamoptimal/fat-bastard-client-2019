import { FETCH_VIDEO } from "../actions/types";

const INITIAL_STATE = {
  videos: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_VIDEO:
      return action.payload;

    default:
      return state;
  }
};
