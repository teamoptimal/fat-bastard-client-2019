import { FETCH_PAGE, FETCH_PAGES } from "../actions/types";

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_PAGE:
      return action.payload;

    case FETCH_PAGES:
      return action.payload.data;

    default:
      return state;
  }
};
