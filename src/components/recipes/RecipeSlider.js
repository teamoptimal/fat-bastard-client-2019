import './Recipes.scss'
import React, { Component } from 'react';
import { connect } from "react-redux";
import RecipeItem from './RecipeItem'
import { fetchRecipes } from "../../actions";
import Slider from "react-slick";


class Recipes extends Component {
  componentDidMount(){
    this.props.fetchRecipes()
  }

  renderRecipes() {
    if(!this.props.recipes){
      return <div>Loading</div>
    }

    return this.props.recipes.map(recipe => {
      return <RecipeItem key={recipe.id} recipe={recipe}/>
    })
  }
  
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      arrows: false,
      fade: true,
      autoplay: true,
      autoplaySpeed: 2000
    };

    return (
       <div className="recipe-slider">
         <Slider {...settings}>{this.renderRecipes()}</Slider>
       </div> 
    );
  }
}

const mapStateToProps = state => {
  return {
    recipes: Object.values(state.recipes),
  };
};

export default connect(
  mapStateToProps,
  {
    fetchRecipes
  }
)(Recipes);
