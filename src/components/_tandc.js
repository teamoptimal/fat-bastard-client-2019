import React from "react";

export default function tandc() {
  return (
    <>
    <div className="container fb_tandc" style={{ marginTop:"120px", marginBottom:"100px" }}>
      <h2 align="center" class="style6">
        <strong style={{fontWeight: 'bold'}}>“FAT bastard Golden Ticket Competition” – Online Shop Competition</strong>
      </h2>
      <ol>
        <li>The competition is open to all residents of South Africa who are aged 18 years or older.</li>
        <li>The Promoter is Robertson Winery (Pty) Ltd, hereinafter referred to as "the Promoter".</li>
        <li>Excluded from entering this competition are employees, directors, members, consultants and partners of the advertising agency or agents of the Promoter, and their spouses, life partners, immediate family, business partners or associates.</li>
        <li>There are 50 golden ticket vouchers that can be won by 50 winners.</li>
        <li>
          Each golden ticket may be one of below prizes:
          <ul>
            <li>FAT bastard online shop discount vouchers (10%, 15%, 20% discount)</li>
            <li>FAT bastard 1.5L Magnums</li>
            <li>FAT bastard Wine Openers</li>
          </ul>
        </li>
        <li>The competition runs from 8am on Tuesday, 22 March to 11:59pm on Saturday, 30 April 2022.</li>
        <li>To enter participants must purchase FAT bastard wine from the FAT bastard online shop. Link: <a href="https://shop.fatbastardwine.co.za/">https://shop.fatbastardwine.co.za/</a></li>
        <li>Users may enter the competition as many times as they like.</li>
        <li>The prizes cannot be transferred or exchanged for its cash value. The prize shown in the promotional material is not an actual representation of the prize.</li>
        <li>The receiver of a golden ticket will need to email <a href="mailto:riekie@robertsonwinery.co.za">riekie@robertsonwinery.co.za</a> and provide proof of a golden ticket to redeem their prize.</li>
        <li>The winners need to supply their daytime delivery address.</li>
        <li>The Promoter’s decision is final, and no correspondence will be entered into.</li>
        <li>By entering the competition and/or accepting the prize, the winners hereby indemnify, releases and holds harmless Robertson Winery (Pty) Ltd. and the Promoter and other, from and against any actions, claims, liability for injury, loss, damage of any kind resulting from the competition and/or prize.</li>
        <li>The Promoter reserves the right to change or cancel the promotion and/or prizes for whatever reason they see fit.</li>
        <li>By entering this competition, you agree to give the Promoter and its group of companies’ permissions to keep all material and information submitted as part of your entry, on electronic or hard copy databases and filing systems for the purpose of the above mentioned competition and by entering this competition, you agree to the Promoter transferring your data to our promotional partners to enable the awarding of the winning prize.</li>
        <li>The Promoter will obtain written consent from the winners should the Promoter wish to use the names, photographs, and entries submitted by the relevant competition winner, for the purposes of unpaid publicity relating to the promotion, by the Promoter.</li>
        <li>Entry instructions are deemed to form part of the Terms and Conditions and by entering this competition all participants will be deemed to have accepted and be bound by the Terms and Conditions. Please retain a copy for your information.</li>
      </ol>
    </div>



    <div className="container fb_tandc" style={{ marginBottom:"100px" }}>
      <h2 align="center" class="style6">
        <strong style={{fontWeight: 'bold'}}>WELCOME TO FAT bastard. </strong>
      </h2>
      <p align="center" class="style6">
        <strong>
          WE SUPPORT RESPONSIBLE DRINKING AND OUR WINES ARE NOT FOR SALE TO
          PERSONS UNDER THE AGE OF 18.{" "}
        </strong>
      </p>


      <p align="center" class="style6"><strong style={{fontWeight: 'bold'}}>“FAT bastard Father’s Day Competition” – Online Competition</strong></p>
      <ol>
          <li>The competition is open to all residents of South Africa who are aged 18 years or older.</li>
          <li>The Promoter is Robertson Winery (Pty) Ltd, hereinafter referred to as "the Promoter".</li>
          <li>Excluded from entering this competition are employees, directors, members, consultants and partners of the advertising agency or agents of the Promoter, and their spouses, life partners, immediate family, business partners or associates.</li>
          <li>
              There are five prizes that can be won by 5 winners: this comprises of:
              <ul>
                  <li>1x Le Creuset Wine Gift Set</li>
                  <li>1 case of FAT bastard The Golden Reserve</li>
              </ul>
          </li>
          <li>The competition runs from 9am on Tuesday 1 June 2021 to 5pm on Wednesday 30 June 2021.</li>
          <li>
              To enter participants must refer 3 Friends to receive the competition email.
              <ul>
                  <li>To enter online Visit www.fatbastardwine.co.za and navigate to the competition page (from your PC or mobile phone). Complete and submit the online form to enter the draw to stand to win.</li>
              </ul>
          </li>
          <li>Users may enter the competition as many times as they like.</li>
          <li>However, if a user has previously won a Robertson Winery or FAT bastard promotion in 2016, 2017, 2018, 2019 or 2020 they will not be eligible to win a prize.</li>
          <li>The prizes cannot be transferred or exchanged for its cash value. The prize shown in the promotional material is not an actual representation of the prize.</li>
          <li>5 Prize winners will be randomly selected (using an automated system) and notified via email by Wednesday the 14th of July 2021.</li>
          <li>The winners will need to provide proof of identity in order to redeem their prize.</li>
          <li>The winners needs to supply their day time delivery address.</li>
          <li>The winners will be announced on the FAT bastard Facebook page.</li>
          <li>The Promoter will make every reasonable attempt to contact the winners. Should we be unable to contact a winner we reserve the right to draw a new winner.</li>
          <li>By entering the competition and/or accepting the prize, the winners hereby indemnify, releases and holds harmless Robertson Winery (Pty) Ltd. and the Promoter and other, from and against any actions, claims, liability for injury, loss, damage of any kind resulting from the competition and/or prize.</li>
          <li>The Promoter’s decision is final, and no correspondence will be entered into.</li>
          <li>The Promoter reserves the right to change or cancel the promotion and/or prizes for whatever reason they see fit.</li>
          <li>By entering this competition, you agree to give the Promoter and its group of companies’ permissions to keep all material and information submitted as part of your entry, on electronic or hard copy databases and filing systems for the purpose of the above mentioned competition and by entering this competition, you agree to the Promoter transferring your data to our promotional partners to enable the awarding of the winning prize.</li>
          <li>The Promoter will obtain written consent from the winners should the Promoter wish to use the names, photographs, and entries submitted by the relevant competition winner, for the purposes of unpaid publicity relating to the promotion, by the Promoter.</li>
          <li>Entry instructions are deemed to form part of the Terms and Conditions and by entering this competition all participants will be deemed to have accepted and be bound by the Terms and Conditions. Please retain a copy for your information.</li>
      </ol>
    </div>
    </>
  );
}
