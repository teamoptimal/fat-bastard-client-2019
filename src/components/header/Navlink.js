import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Navlink extends Component {
  renderLink(){
    
    if(!this.props.to){
      return (
        <div to={this.props.to} className="nav-link p-t-5 p-b-0">
          {this.props.title}
        </div>
      )
    }
    return (
      <Link to={this.props.to} className="nav-link p-t-5 p-b-0">
        {this.props.title}
      </Link>
    )
  }
  render() {
    return (
      this.renderLink()
    );
  }
}
