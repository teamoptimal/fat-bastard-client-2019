import "./Button.scss";

import React from "react";
import { Link } from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'

export default function ButtonRight(props) {
  return (
    <div>
      <Link to={props.toUrl} className={`btnRight ${props.color}`}>
        <div className="inner">
          <span className="button-left">{props.title} </span>
          <span className="button-right">
          
          <FontAwesomeIcon icon={faArrowRight} />
          </span>
        </div>
      </Link>
    </div>
  );
}
