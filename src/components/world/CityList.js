import React, { Component } from 'react'
import { connect } from "react-redux";
import { fetchProvinces, fetchCities } from "../../actions";
import {

  Input,

} from "reactstrap";
class CityList extends Component {
  renderCities(){
    if(!this.props.cities){
      return <option value='1'>1</option>;
    }
    
    const cities = this.props.cities;

    return cities.map(city => {
      return <option key={city.id} data-id={city.id} value={city.id}>{city.name}</option>
    })
  }

  render() {
    return (
      <Input  {...this.props.input} type={this.props.type} ref={this.select}>
       {this.renderCities()}
      </Input>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return { 
    cities: Object.values(state.cities),
   };
};

export default connect(
  mapStateToProps,
  { fetchProvinces, fetchCities }
)(CityList);
