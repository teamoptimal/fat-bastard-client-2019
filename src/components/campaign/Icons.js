import React from 'react'
import Icon from './Icon'

import instagram from './img/instagram-icon@2x.png'
import facebook from './img/facebook-icon@2x.png'
import twitter from './img/twitter-icon@2x.png'
import website from './img/web-icon@2x.png'

class Icons extends React.Component {
    render(){
        return(
            <div className="c_icons">
                <Icon link="https://www.instagram.com/fatbastardsa/" img={instagram} />
                <Icon link="https://www.facebook.com/FATbastardWineSA" img={facebook} />
                <Icon link="https://twitter.com/FATbastardSA" img={twitter} />
                <Icon link="https://www.fatbastardwine.co.za/" img={website} />
            </div>
        )
    }
}

export default Icons