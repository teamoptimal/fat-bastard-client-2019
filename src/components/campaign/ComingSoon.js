import "./ComingSoon.scss";
import React from "react";
import { Helmet } from "react-helmet";
import Banner from "../slider/Banner";
import Contact from "../forms/Contact";

export default function ComingSoon() {
  return (
    <div className="contact-us">
      <Helmet>
        <meta charSet="utf-8" />
        <title>{`FAT bastard wines | Fathersday `}</title>
        <meta
          name="description"
          content="Fathersday Coming Soon"
        />
        <link rel="canonical" href={window.location} />
      </Helmet>

      <Banner
        title="Coming Soon"
        breadcrumbs="HOME / Fathersday Campaign"
        // subtitle={`
        //     <h2>Coming Soon</h2>
        //   `}
      />
      <Contact />
    </div>
  );
}
