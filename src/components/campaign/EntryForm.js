import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";

import {
  Button,
  FormGroup,
  Label,
  Input,
  Col,
  Row,
  CustomInput
} from "reactstrap";

import {Link} from 'react-router-dom'

import Recaptcha from 'react-recaptcha'

import scrollToFirstError from './SmoothScrollToErrorFields.js';

class EntryForm extends Component {

  state = {
    submitted: false,
    capchaLoaded: null,
    capchaVerified: null
  }

  componentDidUpdate(){
    if(this.state.submitted){
      if(this.props.response){
        this.setState({
          submitted: false
        })
      }
    }    
  }

  callback = () => {
    console.log('recapcha has loaded')
    this.setState({
        capchaLoaded: true
    })
  }

  verifyCallback = response => {
      console.log('recapcha has been verified')
      this.setState({
          capchaVerified: true
      })
  }

  renderInput = ({ input, label, meta, type, id, placeholder }) => {

    const hasError = meta.error && meta.touched ? true : false;

    return (
      <FormGroup>
        {
          label && <Label className="c_par-3">{label}</Label>
        }
        <Input {...input} type={type} id={id} invalid={hasError} placeholder={placeholder} />
        {
          hasError && (
            <Error>{meta.error}</Error>
          )
        }
      </FormGroup>
    );
  };

  renderSelectInput({ input, label, meta, id, data }){

    const hasError = meta.error && meta.touched ? true : false;

    if(hasError){
      console.log(meta.error)
    }

    return(
      <FormGroup>
        {
          label && <Label className="c_par-3">{label}</Label>
        }
        <Input {...input}  type="select" id={id}>
          <option value=""></option>
          {
            data.length > 0 && data.map((item, index) => {
              return (
                <option key={index} value={item}>{item}</option>
              )
            })
          }
        </Input>
        {
          hasError && (
            <Error>{meta.error}</Error>
          )
        }
      </FormGroup>
    )
  }

  renderCheckbox({ input, label, meta, id }){

    const hasError = meta.error && meta.touched ? true : false;

    return(
      <FormGroup>
        <div>
          <CustomInput {...input} type="checkbox" id={id} label={label}  className="c_par-3" />
        </div>
        {
          hasError && <Error>{meta.error}</Error>
        }
      </FormGroup>
    )
  }

  onSubmit = formValues => {

    if(this.state.capchaLoaded){
      if(this.state.capchaVerified){
        this.setState({
          submitted: true
        })
        this.props.onSubmit(formValues)
      } else {
        alert('Please verify you are not a robot.')
      }
    }    
  }

  render() {

    const gender = ['male', 'female']

    const provinces = [
      'Western Cape', 'Eastern Cape', 'Free State', 'Gauteng', 'KwaZulu-Natal', 'Limpopo', 'Mpumalanga', 'Northern Cape', 'North West'
    ]

    return (        
          <form
            onSubmit={this.props.handleSubmit(this.onSubmit)}>
            
            <Row>
              <Col md={12}>
                <h3 className="c_heading-4 c_heading">To enter, refer 3 friends and stand a chance to WIN a <strong>FAT <i>bastard</i></strong> wine hamper to the value of R2 000.00.</h3>
              </Col>
            </Row>

            <Row form>
              
              <Col md={6}>
                {'name' && <div name={`position-name`} />}
                <Field
                  id="name"
                  type="text"
                  name="name"
                  component={this.renderInput}
                  label="Name"
                />
              </Col>

              <Col md={6}>
                {'surname' && <div name={`position-surname`} />}
                <Field
                  id="surname"
                  type="text"
                  name="surname"
                  component={this.renderInput}
                  label="Surname"
                />
              </Col>

              <Col md={6}>
                {'email' && <div name={`position-email`} />}
                <Field
                  id="email"
                  type="email"
                  name="email"
                  component={this.renderInput}
                  label="Email Address"
                />
              </Col>

              <Col md={6}>
                {'mobile' && <div name={`position-mobile`} />}
                <Field
                  id="mobile"
                  type="text"
                  name="mobile"
                  component={this.renderInput}
                  label="Mobile Number"
                />
              </Col>

              <Col md={6}>
              {'gender' && <div name={`position-gender`} />}
              <Field
                  id="gender"
                  name="gender"
                  component={this.renderSelectInput}
                  label="Gender"
                  data={gender}
                />
              </Col>

              <Col md={6}>
                {'birthday' && <div name={`position-birthday`} />}
                <Field
                  id="birthday"
                  type="date"
                  name="birthday"
                  component={this.renderInput}
                  label="Birthday"
                />
              </Col>

              <Col md={6}>
                <Field
                  id="province"
                  name="province"
                  component={this.renderSelectInput}
                  label="Province"
                  data={provinces}
                />
              </Col>

              <Col md={6}>
                <Field
                  id="city"
                  type="text"
                  name="city"
                  component={this.renderInput}
                  label="City"
                />
              </Col>
            </Row>

            <Row>
              <Col md={12}>
                <h3 className="c_heading-4 c_heading">Which <strong>FAT <i>bastard</i></strong> wine is your favourite?</h3>
              </Col>
            </Row>
            

            <Row form>
              
              <Col md={3}>
                <Field
                  id="interest_red"
                  name="interest_red"
                  component={this.renderCheckbox}
                  label="Red"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="interest_white"
                  name="interest_white"
                  component={this.renderCheckbox}
                  label="White"
                />
              </Col>
              
              <Col md={3}>
                <Field
                  id="interest_rose"
                  name="interest_rose"
                  component={this.renderCheckbox}
                  label="Rosé"
                />
              </Col>
            </Row>

            <Row>
              <Col md={12}>
                <h3 className="c_heading-4 c_heading">Which social media channels do you use on a regular basis?</h3>
              </Col>
            </Row>

            <Row>
              <Col md={3}>
                <Field
                  id="social_facebook"
                  name="social_facebook"
                  component={this.renderCheckbox}
                  label="Facebook"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="social_twitter"
                  name="social_twitter"
                  component={this.renderCheckbox}
                  label="Twitter"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="social_instagram"
                  name="social_instagram"
                  component={this.renderCheckbox}
                  label="Instagram"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="social_youtube"
                  name="social_youtube"
                  component={this.renderCheckbox}
                  label="YouTube"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="social_whatsapp"
                  name="social_whatsapp"
                  component={this.renderCheckbox}
                  label="WhatsApp"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="social_pinterest"
                  name="social_pinterest"
                  component={this.renderCheckbox}
                  label="Pinterest"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="social_wechat"
                  name="social_wechat"
                  component={this.renderCheckbox}
                  label="WeChat"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="social_snapchat"
                  name="social_snapchat"
                  component={this.renderCheckbox}
                  label="Snapchat"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="social_no_social"
                  name="social_no_social"
                  component={this.renderCheckbox}
                  label="I don't use social meida"
                />
              </Col>
            </Row>

            <Row>
              <Col md={12}>
                <h3 className="c_heading-4 c_heading">Yes, I would like to receive  updates via:</h3>
              </Col>
            </Row>

            <Row className="justify-content-center">
              <Col md={3}>
                <Field
                  id="receive_emails"
                  name="receive_emails"
                  component={this.renderCheckbox}
                  label="Email"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="receive_sms"
                  name="receive_sms"
                  component={this.renderCheckbox}
                  label="SMS"
                />
              </Col>
            </Row>

            <Row>
              <Col md={12}>
                <h3 className="c_heading-4 c_heading">Terms & Conditions:</h3>
              </Col>
            </Row>

            <Row className="justify-content-center">
              <Col md={3}>
                <Field
                  id="over_18"
                  name="over_18"
                  component={this.renderCheckbox}
                  label="I am over 18"
                />
              </Col>
              <Col md={3}>
                <Field
                  id="accept_terms"
                  name="accept_terms"
                  component={this.renderCheckbox}
                  label="Accept Terms & Conditions"
                />
              </Col>
              <Col sm={12} className="text-center">
                <a href="/terms-and-conditions" target="_blank">View Our Terms & Conditions</a>
              </Col>
            </Row>

            <Row>
              <Col md={12}>
                <h3 className="c_heading-4 c_heading">Refer 3 friends:</h3>
              </Col>
            </Row>

            <Row>
              <Col md={4}>
                <Field
                  id="referal_1_name"
                  type="text"
                  name="referal_1_name"
                  component={this.renderInput}
                  placeholder="Name"
                />
                <Field
                  id="referal_1_email"
                  type="text"
                  name="referal_1_email"
                  component={this.renderInput}
                  placeholder="Email"
                />
              </Col>
              <Col md={4}>
                <Field
                  id="referal_2_name"
                  type="text"
                  name="referal_2_name"
                  component={this.renderInput}
                  placeholder="Name"
                />
                <Field
                  id="referal_2_email"
                  type="text"
                  name="referal_2_email"
                  component={this.renderInput}
                  placeholder="Email"
                />
              </Col>
              <Col md={4}>
                <Field
                  id="referal_3_name"
                  type="text"
                  name="referal_3_name"
                  component={this.renderInput}
                  placeholder="Name"
                />
                <Field
                  id="referal_3_email"
                  type="text"
                  name="referal_3_email"
                  component={this.renderInput}
                  placeholder="Email"
                />
              </Col>
            </Row>

            <Row>
              <Col sm={12} className="d-flex justify-content-center">
                <Recaptcha
                    sitekey="6LdkcskZAAAAAL__nmHrjcWIaArZTB8wXvbM4PN0"
                    render="explicit"
                    onloadCallback={this.callback}
                    verifyCallback={this.verifyCallback}
                /> 
              </Col>
              <Col md={12} className="text-center">
                
                <Button
                  className="c_button"
                  type="submit"
                >
                  {
                    this.state.submitted && (
                      <>SUBMITTING YOUR ENTRY...</>
                    )
                  }
                  {
                    !this.state.submitted && (
                      <>SUBMIT</>
                    )
                  }
                  
                </Button>
                {this.props.response && (
                  <>
                    {
                      this.props.response.email && (                        
                        <div className="fail_error">
                          The email has already been submitted
                        </div>
                      )
                    }
                  </>
                  
                )}
              </Col>
            </Row>
          </form>
    );
  }
}

const validate = formValues => {

  const errors = {};

  if (!formValues.name) {
    errors.name = "You must enter your name";
  }
  if (!formValues.surname) {
    errors.surname = "You must enter your surname";
  }
  if (!formValues.email) {
    errors.email = "You must enter your email address";
  } else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formValues.email)){
    errors.email = "You must enter a valid email address";
  }
  if (!formValues.mobile) {
    errors.mobile = "You must enter your mobile number";
  } else if(formValues.mobile.length !== 10){
    errors.mobile = "You must enter a valid mobile number";
  }
  if (!formValues.gender) {
    errors.gender = "You must select a gender";
  }
  if (!formValues.birthday) {
    errors.birthday = "You must enter your date of birth";
  }
  if (!formValues.province) {
    errors.province = "You must select a province";
  }
  if (!formValues.city) {
    errors.city = "You must enter a city";
  }
  if (!formValues.over_18) {
    errors.over_18 = "You must comfirm you are over 18";
  }
  if (!formValues.accept_terms) {
    errors.accept_terms = "You must accept terms and conditions";
  }
  if(formValues.referal_1_email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formValues.referal_1_email)){
    errors.referal_1_email = "You must enter a valid email address";
  }
  if(formValues.referal_2_email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formValues.referal_2_email)){
    errors.referal_2_email = "You must enter a valid email address";
  }
  if(formValues.referal_3_email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(formValues.referal_3_email)){
    errors.referal_3_email = "You must enter a valid email address";
  }

  return errors;
}

const Error = props => {
  return(
    <div className="c_error">
      {props.children}
    </div>
  )
}

export default reduxForm({
  form: "entryForm",
  validate,
  onSubmitFail: (errors) => scrollToFirstError(errors),
})(EntryForm);
