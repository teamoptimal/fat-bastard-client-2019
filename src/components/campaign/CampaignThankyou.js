import React from 'react'
import Thankyou from './Thankyou'
import Footer from '../footer/Footer'
import { Helmet } from "react-helmet";

class CampaignThankyou extends React.Component {
    render(){
        return(
            <>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>{`FAT bastard Wines | Father's Day `}</title>
                    <meta
                    name="description"
                    content="Celebrate FATHER’S DAY in style with a FAT bastard wine hamper! Enter here and stand to WIN."
                    />
                    <link rel="canonical" href="https://www.fatbastardwine.co.za/fathersday" />

                    <meta property="og:title" content="FAT bastard Wines | Father's Day" />
                    <meta property="og:description" content="Celebrate FATHER’S DAY in style with a FAT bastard wine hamper! Enter here and stand to WIN." />
                    <meta property="og:image" content="https://www.fatbastardwine.co.za/fb-fathers-day-social-sharing@2x.jpg" />
                    <meta property="og:url" content="https://www.fatbastardwine.co.za/fathersday" ></meta>

                    <meta name="twitter:title" content="FAT bastard Wines | Father's Day" />
                    <meta name="twitter:description" content="Celebrate FATHER’S DAY in style with a FAT bastard wine hamper! Enter here and stand to WIN." />
                    <meta name="twitter:image" content="https://www.fatbastardwine.co.za/fb-fathers-day-social-sharing@2x.jpg" />
                    <meta name="twitter:card" content="summary_large_image"></meta>

                    <meta property="og:site_name" content="FAT bastard" />
                    <meta name="twitter:image:alt" content="Each hamper consists of a Le Creuset Wine Gift Set and a Case Of FAT bastard The Golden Reserve"></meta>
                </Helmet>
                <Thankyou />
                <Footer />
            </>
        )
    }
}

export default CampaignThankyou