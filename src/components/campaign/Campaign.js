import React from 'react'
import EntryForm from './EntryForm'
import {
  createEntry
} from "../../actions"; 
import {connect} from 'react-redux'
import Section1 from './Section1'
import Section2 from './Section2'
import Section3 from './Section3'
import Section4 from './Section4'
import Footer from '../footer/Footer'
import './Campaign.scss'
import { Helmet } from "react-helmet";
import image from './img/gift-set-02@2x.jpg'

class Campaign extends React.Component {

    onSubmit = formValues => {
        this.props.createEntry(formValues);
    };

    render(){

        return(
            <React.Fragment>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>{`FAT bastard wines | Fathersday `}</title>
                    <meta
                    name="description"
                    content="Celebrate FATHER’S DAY in style with a FAT bastard wine hamper! To enter, refer 3 friends and stand a chance to WIN a FAT bastard wine hamper to the value of R2 000.00."
                    />
                    <link rel="canonical" href={window.location} />

                    <meta property="og:title" content="FAT bastard wines | Fathersday" />
                    <meta property="og:description" content="Celebrate FATHER’S DAY in style with a FAT bastard wine hamper! To enter, refer 3 friends and stand a chance to WIN a FAT bastard wine hamper to the value of R2 000.00." />
                    <meta property="og:image" content={image} />
                    <meta property="og:url" content={window.location} ></meta>

                    <meta name="twitter:title" content="FAT bastard wines | Fathersday" />
                    <meta name="twitter:description" content="Celebrate FATHER’S DAY in style with a FAT bastard wine hamper! To enter, refer 3 friends and stand a chance to WIN a FAT bastard wine hamper to the value of R2 000.00." />
                    <meta name="twitter:image" content={image} />
                    <meta name="twitter:card" content="summary_large_image"></meta>

                    <meta property="og:site_name" content="FAT bastard" />
                    <meta name="twitter:image:alt" content="Each hamper consists of a Le Creuset Wine Gift Set and a Case Of FAT bastard The Golden Reserve"></meta>
                </Helmet>
                <Section1 />
                <Section2 />
                <Section3>
                    <EntryForm 
                        onSubmit={this.onSubmit}
                        response={this.props.status.data ? this.props.status.data.error : null } 
                    />
                </Section3>
                <Section4 />
                <Footer />
            </React.Fragment>
        )
    }
}

const mapStateToprops = state => {
    return{
        status: state.entry
    }
}

export default connect( mapStateToprops, {
    createEntry
})(Campaign)