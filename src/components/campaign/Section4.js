import React from 'react'
import image from './img/fb-red-wine@2x.png'

class Section4 extends React.Component {
    render(){
        return(
            <section id="c_section4" className="c_section c_section4">
                <div className="c_col">
                    <div className="c_overlay"></div>
                    <h2 className="c_heading">Discount on all<br /><strong>FAT <i>bastard</i></strong> red wines</h2>
                    <img className="c_image" src={image} alt="" />
                    <p className="c_par">We are also giving you a discount on all our <strong>FAT <i>bastard</i></strong> red wines for the month of June.</p>
                    <h3 className="c_subheading">DISCOUNT INFO:</h3>

                    <ul className="c_list">
                        <li><strong><span className="themeGold">•</span>&nbsp;&nbsp;Date valid:</strong> 1-30 June 2021</li>
                        <li><strong><span className="themeGold">•</span>&nbsp;&nbsp;Wine:</strong> All <strong>FAT <i>bastard</i></strong> red wines</li>
                        <li><strong><span className="themeGold">•</span>&nbsp;&nbsp;Discount:</strong> 15%</li>
                        <li><strong><span className="themeGold">•</span>&nbsp;&nbsp;Code name:</strong> FATHERSDAY</li>
                    </ul>
                    <a href="https://shop.fatbastardwine.co.za/" className="c_button" >Shop Now</a>
                </div>
            </section>
        )
    }
}

export default Section4