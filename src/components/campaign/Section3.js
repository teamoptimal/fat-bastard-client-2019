import React from 'react'

class Section3 extends React.Component {
    render(){
        return(
            <section id="c_section3" className="c_section c_section3">
                <div className="c_col" >
                    {this.props.children}
                </div>
            </section>
        )
    }
}

export default Section3