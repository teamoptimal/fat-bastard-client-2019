import React from 'react'
import logo from './img/fb-logo@2x.png'
import image from './img/fathers-day-main-entry-page@2x.jpg'

class Section1 extends React.Component {

    scrollTo = anchor => {
        setTimeout(() => { 
            var element = document.getElementById(anchor)
            var headerOffset = 0
            var elementPosition = element.offsetTop
            var offsetPosition = elementPosition - headerOffset

            window.scrollTo({
                top: offsetPosition,
                behavior: "smooth"
            })
        }, 200)
    }
    
    render(){
        return(
            <section id="c_section1" className="c_section c_section1">
                <div className="c_col c_col1">
                    <figure className="c_logo"><img src={logo} alt="" /></figure>
                    <h1 className="c_headline"><i>Celebrate</i> <span className="c_theme-color"><strong>FATHER’S DAY</strong></span> in style with a <span className="c_theme-color"><strong>FAT <i>bastard</i></strong></span> wine hamper!</h1>
                    <p className="c_par">To enter, refer 3 friends and stand a chance to <strong>WIN</strong> a <strong>FAT <i>bastard</i></strong> wine hamper to the value of R2 000.00.</p>
                    <button className="c_button" onClick={() => this.scrollTo('c_section3')}>Enter Here</button>
                </div>
                <div className="c_col c_col2 c_image" style={{
                    backgroundImage: `url(${image})`
                }}></div>
            </section>
        )
    }
}

export default Section1