import React from 'react'

class Icon extends React.Component {
    render(){
        return(            
            <a href={this.props.link} className="c_icon" target="_blank">
                <img src={this.props.img} alt="" width="35" height="35" />
            </a>
        )
    }
}

export default Icon