import React, { Component } from "react";
import { Passers } from "prop-passer";
import {
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
  WhatsappShareButton,

  FacebookIcon,
  TwitterIcon,
  LinkedinIcon,
  WhatsappIcon,
} from "react-share";


class Share extends Component {

  render() {

    const {
      url = window.location.protocol + '//' + window.location.hostname + '/fathersday',
      title = "Father's Day",
      shareImage = window.location.hostname + 'fathers-day-main-entry-page@2x.jpg',
      size = "45px",
      color = '#D6B052'
    } = this.props;

    const bgStyle = {
      fill: color
    }

    const ShareList = Passers({
      url,
      className: "network__share-button",
    })({
      className: "network cursor-pointer hover transition--default",
      title: `Share ${url}`,
    })("li");

    return (
        <ul className="social_share-list">
          <ShareList>
            <FacebookShareButton
              quote={title}
            >
              <FacebookIcon
                size={size}
                round
                bgStyle={bgStyle}
              />
            </FacebookShareButton>

            <TwitterShareButton
              title={title}
            >
              <TwitterIcon
                size={size}
                round
                bgStyle={bgStyle}
              />
            </TwitterShareButton>

            <WhatsappShareButton
              title={title}
              separator=":: "
            >
              <WhatsappIcon 
                size={size} 
                round 
                bgStyle={bgStyle}
              />
            </WhatsappShareButton>

            <LinkedinShareButton
              title={title}
              windowWidth={750}
              windowHeight={600}
            >
              <LinkedinIcon
                size={size}
                round
                bgStyle={bgStyle}
              />
            </LinkedinShareButton>
          </ShareList>
        </ul>
    );
  }
}

export default Share;

