import React from 'react'
import image from './img/gift-set-01@2x.jpg'
import image2 from './img/gift-set-02@2x.jpg'

class Section2 extends React.Component {

    scrollTo = anchor => {
        setTimeout(() => { 
            var element = document.getElementById(anchor)
            var headerOffset = 0
            var elementPosition = element.offsetTop
            var offsetPosition = elementPosition - headerOffset

            window.scrollTo({
                top: offsetPosition,
                behavior: "smooth"
            })
        }, 200)
    }

    render(){
        return(
            <section id="c_section2" className="c_section c_section2">
                <div className="c_col c_col1 c_image" style={{
                    backgroundImage: `url(${image})`
                }}>
                    
                </div>
                <div className="c_col c_col2" >
                    <h2 className="c_subheading"><strong>Each hamper consists of the following:</strong></h2>
                    <ul className="c_list">
                        <li>Le Creuset Wine Gift Set</li>
                        <li>A Case Of <strong>FAT <i>bastard</i></strong> The Golden Reserve</li>
                    </ul>
                    <p className="c_par">Each hamper is worth <strong>R2 000.00.</strong></p>
                    <button className="c_button" onClick={() => this.scrollTo('c_section3')}>Enter Here</button>
                    <img className="c_image2" src={image2} alt="" />
                </div>
            </section>
        )
    }
}

export default Section2