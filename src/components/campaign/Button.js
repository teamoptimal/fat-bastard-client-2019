import React from 'react'
import {Link} from 'react-router-dom'

class Button extends React.Component {
    render(){
        return(
            <Link to={this.props.link} className="c_button">{this.props.text}</Link>
        )
    }
}

export default Button