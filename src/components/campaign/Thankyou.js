import React from 'react'
import logo from './img/fb-logo@2x.png'
import image from './img/fathers-day-main-entry-page@2x.jpg'
import Share from './Share'

class Thankyou extends React.Component {
    
    render(){
        return(
            <section id="c_thankyou" className="c_section c_thankyou">
                <div className="c_col c_col1">
                    <figure className="c_logo"><img src={logo} alt="" /></figure>
                    <h1 className="c_headline">Thank You!</h1>
                    <p className="c_par">
                        Share the competition on social media & get another entry.
                    </p>

                    <Share />           

                    <p className="c_par c_par2">The competition closes on 30 June 2021.</p>

                    <small className="c_tandc"><a href="/terms-and-conditions">View the Terms & Conditions</a></small>

                    <h2 className=" c_subheading"><a href="/">Explore Our Website!</a></h2>
                    
                </div>
                <div className="c_col c_col2 c_image" style={{
                    backgroundImage: `url(${image})`
                }}></div>
            </section>
        )
    }
}

export default Thankyou