import React from 'react'
import Footer from '../footer/Footer'

class CampaignObtOut extends React.Component {
    render(){
        return(
            <>
            <div style={{
                maxWidth: '400px',
                textAlign: 'center',
                margin: '100px auto 100px auto'
            }}>
                <h1>Unsubscribed!</h1>
                <p>You received the campaign email from a friend who referred you as a wine lover. You are not listed on our contactable database and will not received any further communication.</p>
            </div>
            <Footer />
            </>
        )
    }
}

export default CampaignObtOut