import "./Slider.scss";
import React from "react";
import { connect } from "react-redux";
import Slider from "react-slick";
import { Tween } from "react-gsap";
import ReactHtmlParser from "react-html-parser";
import { fetchGallery, clearGallery } from "../../actions";
import ButtonRight from "../buttons/ButtonRight";

//https://github.com/akiran/react-slick

class HomeSlider extends React.Component {
  imageSwap = (imagePAth, alt) => {
    if (window.innerWidth > 800) {
      return;
    } else {
      return 'url("../../resources/Header@2x.jpg")';
    }
  };

  render() {
    var settings = {
      infinite: true,
      speed: 500,
      slidesToShow: 1,

      fade: true,
      autoplay: true
    };

    if (window.innerHeight > 800) {
      settings.dots = false;
      settings.arrows = true;
    } else {
      settings.dots = true;
      settings.arrows = false;
    }

    return (
      <div className="home-slider">
        <Slider {...settings}>
          <div className="sliderItem">
            <div
              style={{
                background: this.imageSwap(),
                backgroundPosition: "right center",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
                height: "100%"
              }}
            >
              <div className="ui container" style={{ height: "100%" }}>
                <header className="sliderHeader">
                  <Tween
                    from={{ y: -20, opacity: 0 }}
                    delay=".2"
                    duration=".7"
                    ease="Circ.easeOut"
                  >
                    <div className="slider-product-name">
                      <span>CHENIN BLANC</span>
                    </div>
                  </Tween>

                  <Tween
                    from={{ x: -20, opacity: 0 }}
                    delay=".4"
                    duration=".7"
                    ease="Circ.easeOut"
                  >
                    {ReactHtmlParser(
                      `<h2 style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">A FAT <em>bastard</em> AS <br>LIVELY AS A GOLDEN<br>RAY OF SUNSHINE.</h2>`
                    )}
                  </Tween>
                </header>

                <Tween
                  from={{ x: 30, opacity: 0 }}
                  delay=".4"
                  duration=".7"
                  ease="Circ.easeOut"
                >
                  <div className="stuff" />
                </Tween>

                <div className="sliderImageOverlay">
                  <div className="inner">
                    <Tween
                      from={{ opacity: 0, rotation: -10 }}
                      delay=".7"
                      duration=".7"
                      ease="Circ.easeOut"
                    >
                      <img
                        className="sun"
                        src="../../resources/img/Sun@2x.png"
                        alt=""
                      />
                    </Tween>
                    <Tween
                      from={{ x: 50, opacity: 0 }}
                      to={{ x: "-20%", opacity: 1 }}
                      delay=".9"
                      duration="1"
                      ease="Circ.easeOut"
                    >
                      <img
                        className="bottle"
                        src="../../resources/img/Layer-0@2x.png"
                        alt=""
                      />
                    </Tween>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div
            className="sliderItem"
            style={{
              background: 'url("../../resources/Header@2x.jpg")',
              backgroundPosition: "center center",
              backgroundSize: "cover"
            }}
          >
            <div
              style={{
                background: 'url("../../resources/Header@2x.jpg")',
                backgroundPosition: "right center",
                backgroundSize: "cover",
                height: "100%"
              }}
            >
              <div className="ui container" style={{ height: "100%" }}>
                <header className="sliderHeader">
                  <Tween
                    from={{ y: -20, opacity: 0 }}
                    delay=".2"
                    duration=".7"
                    ease="Circ.easeOut"
                  >
                    <div className="slider-product-name">
                      <span>CHARDONNAY</span>
                    </div>
                  </Tween>

                  <Tween
                    from={{ x: -20, opacity: 0 }}
                    delay=".4"
                    duration=".7"
                    ease="Circ.easeOut"
                  >
                    {ReactHtmlParser(
                      `<h2 style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">THIS IS THE <br> GRAND-DADDY OF <br> ALL FAT <i>bastards</i>.</h2>`
                    )}
                  </Tween>
                </header>

                <Tween
                  from={{ x: 30, opacity: 0 }}
                  delay=".4"
                  duration=".7"
                  ease="Circ.easeOut"
                >
                  <div className="stuff" />
                </Tween>

                <div className="ChardsliderImageOverlay">
                  <div className="inner">
                    <Tween
                      from={{ opacity: 0, rotation: -10 }}
                      delay=".7"
                      duration=".7"
                      ease="Circ.easeOut"
                    >
                      <img
                        className="chard"
                        src="./resources/img/slider-02-new (1).png"
                        alt=""
                      />
                    </Tween>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Slider>

        <div className="ui container">
          <Tween
            from={{ y: 20, opacity: 0 }}
            duration=".7"
            delay=".7"
            ease="Circ.easeOut"
          >
            <div>
              <ButtonRight
                toUrl="/the-fat-bastards"
                color="yellow"
                title="View Our products"
              />
            </div>
          </Tween>
        </div>
      </div>
    );
  }
}

export default HomeSlider;
