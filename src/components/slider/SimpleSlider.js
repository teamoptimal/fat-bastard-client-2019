import "./SimpleSlider.scss";
import React from "react";

import { connect } from "react-redux";
import { fetchGallery, clearGallery } from "../../actions";

import Slider from "react-slick";
import ReactHtmlParser from "react-html-parser";
import { Tween } from "react-gsap";
import { FAT_BASTARD_API_IMAGE_PATH } from '../../utils/paths';

//https://github.com/akiran/react-slick

class SimpleSlider extends React.Component {
  componentDidMount() {
    this.props.fetchGallery(this.props.pageId, "slider");
  }

  componentWillUnmount() {
    this.props.clearGallery();
  }

  renderContent() {
    if (!this.props.images) {
      return <div>loading</div>;
    }

    return this.props.images.map(img => {
      const imgPath = img.path.replace(/\\/g, "/");
      return (
        <React.Fragment key={img.id}>
          <div
            key={img.id}
            className="sliderItem"
            style={{
              background: `url(${FAT_BASTARD_API_IMAGE_PATH}${imgPath})`,
              backgroundPosition: "center center",
              backgroundSize: "cover"
            }}
          >
            <div className="ui container">
              <header className="SimpleSliderHeader leftaligned">
                <Tween
                  from={{ y: -20, opacity: 0 }}
                  delay=".2"
                  duration=".4"
                  ease="Linear.easeIn"
                >
                  <div className="slider-product-name">
                    <span>HOME / THE FAT BASTARDS</span>
                  </div>
                </Tween>

                <Tween
                  from={{ x: -20, opacity: 0 }}
                  delay=".4"
                  duration=".4"
                  ease="Linear.easeIn"
                >
                  {ReactHtmlParser(img.title)}
                </Tween>
                <Tween
                  from={{ x: 30, opacity: 0 }}
                  delay=".4"
                  duration=".4"
                  ease="Linear.easeIn"
                >
                  <div className="slider-subtitle">
                    {ReactHtmlParser(img.subtitle)}
                  </div>
                </Tween>
              </header>
            </div>
          </div>
        </React.Fragment>
      );
    });
  }

  render() {
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
     
      fade: true,
      autoplay: true,
      autoplaySpeed: 500
    };

    return <Slider {...settings}>{this.renderContent()}</Slider>;
  }
}

const mapStateToProps = state => {
  return {
    images: state.sliderGallery.images
  };
};

export default connect(
  mapStateToProps,
  {
    fetchGallery,
    clearGallery
  }
)(SimpleSlider);
