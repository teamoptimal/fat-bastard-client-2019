import './Gallery.scss'
import React, { Component } from 'react'
import  { connect } from 'react-redux'

import { fetchPageGallery } from '../../actions';
import GalleryItem from './GalleryItem';
import Slider from "react-slick";

class GallerySlider extends Component{
  componentDidMount() {
    this.props.fetchPageGallery(this.props.pageId, "standalone");
  }

  renderGallery(){
    if(!this.props.images){
      return <div>Loading</div>
    }

    return (
      this.props.images.map((image) => {
        return <GalleryItem key={image.id} image={image} />
      })
    )
  }

  render() {
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      arrows: false,
      fade: true,
      autoplay: true,
      autoplaySpeed: 2000
    };

    
    return (
      <div className="gallery-slider">
        <Slider {...settings}>{this.renderGallery()}</Slider>
      </div> 
    )
  }
}

const mapStateToProps = state => {
  return {
    images: state.pageGallery.images,
  };
};

export default connect(
  mapStateToProps,
  {
    fetchPageGallery,
  }
)(GallerySlider);