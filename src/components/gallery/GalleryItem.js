import React from 'react'
import { FAT_BASTARD_API_IMAGE_PATH } from '../../utils/paths';
export default function GalleryItem({ image }) {
  return (
    <div className="gallery-item">
      <img src={`${FAT_BASTARD_API_IMAGE_PATH}${image.path}`} alt={image.alt_description}/>
    </div>
  )
}
