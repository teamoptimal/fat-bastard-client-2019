import React from 'react'
import './Disclaimer.scss'
import { veryfyDisclaimer } from '../../actions'
import { connect } from 'react-redux'
import iconmonstrx from '../../resources/img/iconmonstr-x-mark-2-240.png'

class Disclaimer extends React.Component{

    componentDidMount(){
        const verified = localStorage.getItem('disclaimerIsVerified')
        verified && this.props.veryfyDisclaimer(verified)
    }

    veryfy = () => {
        this.props.veryfyDisclaimer(true)
        localStorage.setItem('disclaimerIsVerified', true)  
    }
    
    render(){
        return (
            <>
                {
                    !this.props.isVerified && (
                        <div className="fb_disclaimer-wrapper fb_shop-popup">
                            <img className='_logo' src="../../resources/img/Fat Bastard@2x.png" alt="" />
                            <h1>LIVE <i>large</i> this May!</h1>
                            <p>My full-bodied <strong>Fb Chardonnay</strong> deserves more than just a single day of celebrations. So this May I’m giving <strong>#ChardonnayDay</strong> the <strong>#LiveLarge</strong> treatment - <strong>15% discount until end of May</strong>.</p>
                            <button onClick={this.veryfy} className="fb_close-button">
                                <img src={iconmonstrx} alt="" />
                            </button>
                            <a href="https://shop.fatbastardwine.co.za" className='btn purple'><i>Shop Now</i></a>
                        </div>
                    )
                }                
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        isVerified: state.disclaimerVerified
    }
}

export default connect(mapStateToProps, {
    veryfyDisclaimer
})(Disclaimer)